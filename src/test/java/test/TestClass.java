package test;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.BasePage;
import test.base.BaseTest;

import static org.assertj.core.api.Assertions.assertThat;

public class TestClass extends BaseTest {
    @Test(testName = "Example Test", description = "Example Test")
    public void testExample() {
        BasePage basePage = new BasePage(getDriver());
        assertThat("aaaa").contains("a");
    }
}
