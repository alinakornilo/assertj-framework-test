package test.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import static pages.BasePage.homeURL;

public class BaseTest {
    private static final String hub = System.getProperty("hub");
    private WebDriver driver = null;

    private void setDriver() {
        System.setProperty("webdriver.chrome.driver",  new File(BaseTest.class.getResource("/chromedriver.exe").getFile()).getPath());
        driver = new ChromeDriver();
    }

    protected WebDriver getDriver() {
        return driver;
    }

    @BeforeClass
    public void setUp() throws Exception {
        setDriver();
        driver.manage().window().maximize();
        driver.get(homeURL);
    }

    @AfterClass
    public void quit() {
        driver.quit();
    }
}