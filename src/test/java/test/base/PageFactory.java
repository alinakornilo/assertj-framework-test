package test.base;

import org.testng.annotations.Factory;
import test.TestClass;

public class PageFactory {
    @Factory
    public Object[] createInstances() {
        return new Object[]{ new TestClass() };
    }
}
